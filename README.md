# About

Docker Compose setup for magento 2.x project

# Requirements

+ Docker Native 
	-	[OSX/WINDOWS](https://www.docker.com/community-edition)
+ Virtual Box 
	- [OSX/WINDOWS](https://www.virtualbox.org/wiki/Downloads)

# Installation

+ `git clone git@bitbucket.org:rhysmurray/docker-mage-2.git <project-name>`
+ Copy magento credentials to ./config/php/config/auth.json (found in your profile on magento's website
+ `docker-compose up -d`
+ Copy project files to ./mage/html/ or install magento 2 with `create-project --repository-url=https://repo.magento.com/ magento/project-community-edition`
+ Copy database to container 127.0.0.1:33006 (this set not need for fresh install)
	+ 	user: root 
	+  pw: root 
	+  database : magento
+   Navigate to [127.0.0.1:8000](127.0.0.1:8000)


# Usage

## bin/magneto

+ `docker-compose exec <container-name> bash` (default install will container will be web)
+ `php bin/magento your:command:here`

## MySQL

You can use the adminer web interface to manage mysql Navigate to [127.0.0.1:8001](127.0.0.1:8001)

You can also use desktop apps like [Sequel Pro](https://www.sequelpro.com/)

## Redis

[Info here](http://devdocs.magento.com/guides/v2.2/config-guide/redis/redis-pg-cache.html)

The server will be "redis"

## Mail

All mail sent through php/smtp will be captured via mailhog.

[View Here](http://127.0.0.1:8025/)


## Pestle

A collection of command line programs, with a primary focus on Magento 2 code generation [Learn More] (https://github.com/astorm/pestle)
 
`docker-compose exec <web-container-name> bash`

`cd bin`

`curl -LO http://pestle.pulsestorm.net/pestle.phar`
